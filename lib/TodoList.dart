import 'package:flutter/material.dart';
import './Todo.dart';

typedef TodoToggleCallback = void Function(bool, Todo);

class TodoList extends StatelessWidget{

   TodoList({@required this.todos, this.onTodoToggle});

   final List<Todo> todos;
   final TodoToggleCallback onTodoToggle;

   Widget _buildItem(BuildContext context, int index){

    final Todo todo = todos[index];
    return CheckboxListTile(
      value : todo.isDone,
      title : Text(todo.title),
      onChanged : (bool isChecked){
        onTodoToggle(isChecked, todo);
        print(onTodoToggle is TodoToggleCallback);
      });
  }
  @override
  Widget build(BuildContext context){
    return ListView.builder(
      itemCount : todos.length,
      itemBuilder : _buildItem
    );
  }
}