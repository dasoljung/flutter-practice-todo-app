import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import './TodoListScreen.dart';

void main(){
  SystemChrome.setEnabledSystemUIOverlays([]); // hide status bar
  runApp(MyApp());
}

class MyApp extends StatelessWidget{
  @override
  Widget build(BuildContext context){
    return MaterialApp(
      title : "Todo In Flutter",
      home : TodoListScreen()
    );
  }
}